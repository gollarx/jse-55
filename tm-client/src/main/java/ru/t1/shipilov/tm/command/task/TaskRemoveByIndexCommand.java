package ru.t1.shipilov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.shipilov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-remove-by-index";

    @NotNull
    private final String DESCRIPTION = "Remove task by index.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken());
        request.setIndex(index);
        taskEndpoint.removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
