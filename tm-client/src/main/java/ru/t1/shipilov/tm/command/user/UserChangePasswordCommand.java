package ru.t1.shipilov.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.UserChangePasswordRequest;
import ru.t1.shipilov.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "change-user-password";

    @NotNull
    private final String DESCRIPTION = "Change password of current user.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);
        userEndpoint.changeUserPassword(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
