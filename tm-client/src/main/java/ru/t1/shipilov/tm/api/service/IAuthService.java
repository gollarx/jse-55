package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    UserDTO getUser();

    void checkRoles(@Nullable Role[] roles);

}
