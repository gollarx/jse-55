package ru.t1.shipilov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.api.endpoint.*;
import ru.t1.shipilov.tm.api.service.*;
import ru.t1.shipilov.tm.api.service.dto.*;
import ru.t1.shipilov.tm.api.service.model.IProjectService;
import ru.t1.shipilov.tm.api.service.model.IProjectTaskService;
import ru.t1.shipilov.tm.api.service.model.ITaskService;
import ru.t1.shipilov.tm.api.service.model.IUserService;
import ru.t1.shipilov.tm.endpoint.*;
import ru.t1.shipilov.tm.service.*;
import ru.t1.shipilov.tm.service.dto.*;
import ru.t1.shipilov.tm.service.model.ProjectService;
import ru.t1.shipilov.tm.service.model.ProjectTaskService;
import ru.t1.shipilov.tm.service.model.TaskService;
import ru.t1.shipilov.tm.service.model.UserService;
import ru.t1.shipilov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@NoArgsConstructor
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void registryEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void start() {
        registryEndpoints();
        initPID();
        loggerService.initJmsLogger();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}
