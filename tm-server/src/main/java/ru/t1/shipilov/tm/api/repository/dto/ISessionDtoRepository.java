package ru.t1.shipilov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {

    boolean existsById(@NotNull String id);

    void remove(@NotNull SessionDTO model);

}

